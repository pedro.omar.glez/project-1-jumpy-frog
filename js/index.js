const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
const canvas2 = document.querySelector('#canvas2');
const ctx2 = canvas2.getContext('2d');
const soundDrop = "drop";
const soundLand = "land";
const soundFrog = "frog";
const soundMusic = "music";
/* ========= Variables ================= */

let lilyPads1 = [], lilyPads2 = [];
let interval;
let frames = 0;
let score1 = 0, score2 = 0;
let radius = 150;
let level1 = 1,level2 = 1;
let direction1 = -1,direction2 = -1;
let xMove1 = 0, xMove2 = 0;
let yMove1 = 0, yMove2 = 0;
let animationSteps1 = 0, animationSteps2 = 0;
let secondsLeft1 = 15, secondsLeft2 = 15;
let spinFrames1 = 0, spinFrames2 = 0;
let live1 = true,live2 = true;
let isPaused = false;
let numberOfPlayers = 2;
let multiplayer = true;

/* ========= funciones principales ================= */
function update(){
  frames++;
  clearCanvas();
  generateLilyPad();
  background.draw();
  drawLilyPad();
  if(frog1 && live1){update1()}
  if(frog2 && live2){update2()}
  drawScore();
  drawSecondsLeft();
  gameOver();
}
function update1(){
  if(animationSteps1>0){lilyAnimation1()};
  frog1.draw();
  frog1.drawGhost(level1,direction1);   
  spinFrames1++;
}
function update2(){
  if(animationSteps2>0){lilyAnimation2()};
  frog2.draw2();
  frog2.drawGhost2(level2,direction2);   
  spinFrames2++;
}
function clearCanvas(){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
}
function start() {
  if(interval) return;
  interval = setInterval(update, 1000/60);
  players(numberOfPlayers);
  let wLily;
  if(numberOfPlayers >= 1){
    wLily = new waterLily(frog1.x + frog1.width/2,frog1.y + frog1.height/2)
    lilyPads1.push(wLily);
  }
  if(numberOfPlayers == 2){
    lilyPads2.push(wLily);
  }
  playSoundMusic();
}
function loadScreen() {
  loadSounds();
  ctx.fillStyle = 'white';
  ctx.font = '60px monospace';
  ctx.fillText("Jumpy Frog", 80, 150);
  ctx.font = '20px monospace';
  ctx.fillText("Press enter to start the game", 80, 300);
  ctx2.fillStyle = 'white';
  ctx2.font = '60px monospace';
  ctx2.fillText("Jumpy Frog", 80, 150);
  ctx2.font = '20px monospace';
  ctx2.fillText("Press enter to start the game", 80, 300);
}
function deadOrAlive1(){
  if(frog1.isInWaterLily(lilyPads1[1])){jump1()}
  else{gameOver1()}
}
function deadOrAlive2(){
  if(frog2.isInWaterLily(lilyPads2[1])){jump2()}
  else{gameOver2()}
}
/* ========= funciones auxiliares ================= */
/* ---------- LilyPad ------------- */
function generateLilyPad(){
  while(10 > lilyPads1.length){
    lilyPads1.push(generateLily(lilyPads1));
  }
  while(10 > lilyPads2.length){
    lilyPads2.push(generateLily(lilyPads2));
  }
}
function generateLily(lilyPads){
  //random grades for the next lily pad between 30 and 150 grades
  let randomGrades = Math.floor(Math.random() * 120) + 30;
  let radians = gradesToRadians(randomGrades); 
  //calculate next x and y postions
  let idx = lilyPads.length - 1;//the index of the previous lilyPad
  let xLily = lilyPads[idx].xCenter - (radius*Math.cos(radians));
  let yLily = lilyPads[idx].yCenter - (radius*Math.sin(radians));
  return (new waterLily(xLily,yLily));
}
function drawLilyPad() {
  lilyPads1.forEach((waterLily) => {
    waterLily.draw();
  });
  lilyPads2.forEach((waterLily) => {
    waterLily.draw2();
  });
}
function lilyAnimation1(){
  lilyPads1.forEach((waterLily) => {
    waterLily.newCenter(xMove1,yMove1);
  });
  animationSteps1--;
}
function lilyAnimation2(){
  lilyPads2.forEach((waterLily) => {
    waterLily.newCenter(xMove2,yMove2);
  });
  animationSteps2--;
}
/* ---------- Draw Score and level ------------- */
function drawScore() {
  ctx.fillStyle = 'white';
  ctx.font = '20px Avenir';
  ctx.fillText("Level "+Math.floor(level2),canvas.width/16,canvas.height/16);
  ctx.font = '40px Avenir';
  ctx.fillText(score1, canvas.width/2, canvas.height/8);
  ctx2.fillStyle = 'white';
  ctx2.font = '20px Avenir';
  ctx2.fillText("Level "+Math.floor(level2),canvas2.width/16,canvas2.height/16);
  ctx2.font = '40px Avenir';
  ctx2.fillText(score2, canvas2.width/2, canvas2.height/8);
}
function drawSecondsLeft() {
  //player1
  ctx.fillStyle = 'white';
  ctx.font = '20px Avenir';
  ctx.fillText("Seconds left "+Math.floor(secondsLeft1)+"s",canvas.width/16,canvas.height*2/16);
  if(spinFrames1 % 60 == 0){
    secondsLeft1--;
  }
  if(secondsLeft1 == 0){gameOver1()};
  //player2
  ctx2.fillStyle = 'white';
  ctx2.font = '20px Avenir';
  ctx2.fillText("Seconds left "+Math.floor(secondsLeft2)+"s",canvas.width/16,canvas.height*2/16);
  if(spinFrames2 % 60 == 0){
    secondsLeft2--;
  }
  if(secondsLeft2 == 0){gameOver2()};
}
//players actions and status
function jump1() {
  secondsLeft1 = 15; //reboots the secondsLeft
  spinFrames1 = 0; // reboots the frame counter for the spins
  let previousWaterLily = lilyPads1.shift();
  xMove1 = (previousWaterLily.xCenter - lilyPads1[0].xCenter)/10;
  yMove1 = (previousWaterLily.yCenter - lilyPads1[0].yCenter)/10;
  animationSteps1 = 10; 
  score1++;
  direction1 = Math.random() < 0.5 ? -1 : 1;
  if(score1%10 == 0){
    level1 += 0.5;
    playSoundFrog();
    
  }else{
    playSoundLand();
  }
}
function jump2() {
  secondsLeft2 = 15; //reboots the secondsLeft
  spinFrames2 = 0; // reboots the frame counter for the spins
  let previousWaterLily = lilyPads2.shift();
  xMove2 = (previousWaterLily.xCenter - lilyPads2[0].xCenter)/10;
  yMove2 = (previousWaterLily.yCenter - lilyPads2[0].yCenter)/10;
  animationSteps2 = 10; 
  score2++;
  direction2 = Math.random() < 0.5 ? -1 : 1;
  if(score2%10 == 0){
    level2 += 0.5;
    playSoundFrog();
    
  }else{
    playSoundLand();
  }
}
function gameOver(){
  //playSoundDrop();
  if(!(live1 || live2)){
    stopSoundMusic();
    clearInterval(interval);
    ctx.fillStyle = 'white';
    ctx.font = '40px monospace';
    ctx2.fillStyle = 'white';
    ctx2.font = '40px monospace';
    if(multiplayer){  
      if(score1 != score2){
        //player1
        ctx.fillText((score1 > score2) ? "You Win" : "You Loose", 80, 400);
        //player2
        ctx2.fillText((score2 > score1) ? "You Win" : "You Loose", 80, 400);
      }else{
        ctx.fillText("It's a Tie", 80, 400);
        ctx2.fillText("It's a Tie", 80, 400);
      }
    } 
  }
  if(!live1){
    ctx.fillStyle = 'white';
    ctx.font = '60px monospace';
    ctx.fillText("Game Over", 80, 150);
    ctx.font = '20px monospace';
    ctx.fillText("Score: "+score1, 80, 300);
  }
  if(!live2){
    ctx2.fillStyle = 'white';
    ctx2.font = '60px monospace';
    ctx2.fillText("Game Over", 80, 150);
    ctx2.font = '20px monospace';
    ctx2.fillText("Score: "+score2, 80, 300);
  }
}
function gameOver1() {
  live1 = false;
}
function gameOver2() {
  live2 = false;
}
//functions to Maths, restart and pause
function restart() {
  lilyPads1 = [], lilyPads2 = [];
  clearInterval(interval);
  interval = null;
  frames = 0;
  score1 = 0, score2 = 0;
  level1 = 1,level2 = 1;
  direction1 = -1,direction2 = -1;
  xMove1 = 0, xMove2 = 0;
  yMove1 = 0, yMove2 = 0;
  animationSteps1 = 0, animationSteps2 = 0;
  secondsLeft1 = 15, secondsLeft2 = 15;
  spinFrames1 = 0, spinFrames2 = 0;
  live1 = true
  if(multiplayer){live2 = true};
  background = new Background();
  frog1 = null;
  frog2 = null;
  isPaused = false;
  if(!sM){playSoundMusic()};
  stopSoundMusic();
}
function pause() {
  if(!isPaused){
    clearInterval(interval);
    isPaused = true;
    stopSoundMusic();
    ctx.fillStyle = 'white';
    ctx.font = '60px monospace';
    ctx.fillText("Paused", 80, 400);
    ctx2.fillStyle = 'white';
    ctx2.font = '60px monospace';
    ctx2.fillText("Paused", 80, 400);
  }else{
    interval = setInterval(update, 1000/60);
    isPaused = false;
    playSoundMusic();
  }
}
function gradesToRadians(grades) {
  return grades * Math.PI / 180;
}
/* ================== Music ================ */
let sM
function loadSounds () {
  createjs.Sound.registerSound("./sounds/fall.wav", soundDrop);
  createjs.Sound.registerSound("./sounds/landing.wav", soundLand);
  createjs.Sound.registerSound("./sounds/frog.wav", soundFrog);
  createjs.Sound.registerSound("./sounds/bgMusic.wav",soundMusic);
}
function playSoundDrop () {
  const sD = createjs.Sound.play(soundDrop);
  sD.volume = 0.5;
}
function playSoundLand () {
  const sL = createjs.Sound.play(soundLand);
  sL.volume = 0.5;
}
function playSoundFrog () {
  const sF = createjs.Sound.play(soundFrog);
  sF.volume = 0.5;
}
function playSoundMusic() {
  sM = createjs.Sound.play(soundMusic, {interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
  sM.volume = 0.4;
}
function stopSoundMusic() {
  sM.stop();
}