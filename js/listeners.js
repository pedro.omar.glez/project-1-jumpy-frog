document.addEventListener('keydown', event => {
    // React to user pressing a key
    switch (event.keyCode) {
      case 32:
        //console.log("spacebar");
        //jum the flog of the first player
        deadOrAlive1();
        return;
      case 38:
        //console.log("up arrow")
        //jump the frog of the second player
        deadOrAlive2();
        return;
      case 13:
        //console.log("enter");
        //starts the game
        start();
        return;
      case 8:
        restart();
        return;
      case 16:
        pause();
        return;
    }
  });
  const btn1Player = document.querySelector('.players #player1');
  const btn2Player = document.querySelector('.players #player2')
  const divCanvas1 = document.querySelector('div #canvas').parentNode;
  const divCanvas2 = document.querySelector('div #canvas2').parentNode;

  btn1Player.addEventListener('click', () => {
    divCanvas2.style.display = "none";
    divCanvas1.style.width = "80%";
    restart();
    live2 = false;
    multiplayer = false;
  });
  btn2Player.addEventListener('click', () => {
    divCanvas2.style.display = "block";
    divCanvas1.style.width = "40%"; 
    restart();
    live2 = true;
    multiplater = true;
  });

