class Background {
  constructor() {
    this.x = 0;
    this.y = 0;
    this.width = canvas.width;
    this.height = canvas.height;
    this.img = new Image();
    this.img.src = "./images/background.png";
    this.img.onload = () => {
      this.draw();
      loadScreen();
    }
  }
  draw() {
    this.y++;
    if(this.y > this.height){this.y = 0};
    ctx.drawImage(this.img,this.x,this.y,this.width,this.height);
    ctx.drawImage(this.img,this.x,this.y -this.height,this.width,this.height)
    ctx2.drawImage(this.img,this.x,this.y,this.width,this.height);
    ctx2.drawImage(this.img,this.x,this.y -this.height,this.width,this.height)
  }
}
class Frog {
  constructor() {
    this.x = 225;
    this.y = 550;
    this.width = 50;
    this.height = 50;
    this.img = new Image();
    this.img.src = "./images/froggy.png";
    this.radius = 150;
    this.theta=0;
    this.xGhost = (this.x) + this.radius * Math.cos(this.theta);
    this.yGhost = (this.y) + this.radius * Math.sin(this.theta);
  }
  draw(){
    ctx.drawImage(this.img,this.x,this.y,this.width,this.height);
  }
  drawGhost(level,direction) {
    this.theta += 0.015708*level*direction;
    this.xGhost = (this.x) + this.radius * Math.cos(this.theta);
    this.yGhost = (this.y) + this.radius * Math.sin(this.theta);
    if(this.theta > (2*Math.PI)){this.theta = 0};
    ctx.drawImage(this.img,this.xGhost,this.yGhost,this.width,this.height);
  }
  draw2(){
    ctx2.drawImage(this.img,this.x,this.y,this.width,this.height);
  }
  drawGhost2(level,direction) {
    this.theta += 0.015708*level*direction;
    this.xGhost = (this.x) + this.radius * Math.cos(this.theta);
    this.yGhost = (this.y) + this.radius * Math.sin(this.theta);
    if(this.theta > (2*Math.PI)){this.theta = 0};
    ctx2.drawImage(this.img,this.xGhost,this.yGhost,this.width,this.height);
  }
  isInWaterLily(waterLily) {
    let xCenter= this.xGhost + (this.width/2);
    let yCenter= this.yGhost + (this.height/2)
    let a = xCenter - waterLily.xCenter;
    let b = yCenter - waterLily.yCenter;
    let distanceBetween = Math.sqrt( a*a + b*b );
    return (waterLily.radius >= distanceBetween);
  }
}

class waterLily{
  constructor(xCenter,yCenter){
    this.width = 80;
    this.height = 80;
    this.radius = 40;
    this.xCenter = xCenter;
    this.yCenter = yCenter;
    this.x = this.xCenter - (this.width/2);
    this.y = this.yCenter - (this.height/2);
    this.img = new Image();
    this.img.src = "./images/waterLily.png";
  }
  draw() {
    ctx.drawImage(this.img,this.x,this.y,this.width,this.height);
  }
  draw2(){
    ctx2.drawImage(this.img,this.x,this.y,this.width,this.height);
  }
  newCenter(xMove,yMove) {
    this.xCenter += xMove;
    this.yCenter += yMove;;
    this.x = this.xCenter - (this.width/2);
    this.y = this.yCenter - (this.height/2);
  }
}
