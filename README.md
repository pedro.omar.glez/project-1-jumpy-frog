# Project-1 - Jumpy-Frog

**Jumpy Frog** is a videogame made in HTML, CSS  and JavaScript. Challenge your friends for the best score.
You can play the game <a href="https://pedro.omar.glez.gitlab.io/project-1-jumpy-frog/">here</a>

You can see my presentation in <a href="https://slides.com/omargonzalez-2/deck/live#/">here</a>

About the game
============

You are a frog that has to find the promised land of free flies. It's an endless road of lilypads, and you have to jump once at a time


To do list
============

* Implement an animation when the frog jumps.
* Add best scores in local.
* Change the color for the ghost frog and de player 2 (frog and ghost).  


Credits
============

* The Background and sound effects was taken from <a href="https://opengameart.org/">opengameart.org</a>
* The Frog and the Water Lily was made by <a href="https://gitlab.com/pedro.omar.glez">Omar González</a>
* The music was made by my Friend and colleague Emmanuel Cano Félix
